//
//  GithubAPI.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import Siesta
import SwiftyJSON

final class GithubAPI {
    
    private let service = Service(baseURL: "https://api.github.com")
    
    private struct Endpoints {
        static let singleRepo = "/search/repositories"
        static let pullRequests = "/repos/*/*/pulls"
    }
    
    init() {
        service.configure("**") { config in
            config.useNetworkActivityIndicator()

            config.pipeline[.parsing].add(self.jsonParser, contentTypes: ["*/json"])
            
            config.pipeline[.cleanup].add(GithubErrorHandler())
        }
        
        service.configureTransformer(Endpoints.singleRepo) {
            try self.failableAdapt(using: RepositoriesAdapter(input: $0.content as JSON))
        }
        
        service.configureTransformer(Endpoints.pullRequests) {
            try self.failableAdapt(using: PullRequestsAdapter(input: $0.content as JSON))
        }
    }
    
    func repositories(by page: Int) -> Resource {
        return service
            .resource("search/repositories")
            .withParam("q", "language:Swift")
            .withParam("page", "\(page)")
            .withParam("sort", "stars")
    }
    
    func pullRequests(by owner: String, onRepo repo: String) -> Resource {
        return service
            .resource("/repos")
            .child(owner.lowercased())
            .child(repo.lowercased())
            .child("pulls")
    }
    
    private let jsonParser = ResponseContentTransformer { JSON($0.content as AnyObject) }
    
    private struct GithubErrorHandler: ResponseTransformer {
        
        func process(_ response: Response) -> Response {
            switch response {
            case .success:
                return response
            case .failure(var error):
                error.userMessage = error.jsonDict["message"] as? String ?? error.userMessage
                return .failure(error)
            }
        }
    }
    
    private func failableAdapt<T>(using adapter: Adapter<JSON, T>) throws -> T {
        let result = adapter.adapt()
        switch result {
        case .success(let entity):
            return entity
        case .error(let error):
            throw error
        }
    }
}
