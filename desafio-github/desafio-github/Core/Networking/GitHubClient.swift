//
//  GithubClient.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import Siesta

extension Siesta.Resource {
    
    var error: APIError {
        if let underlyingError = self.latestError {
            return .http(underlyingError)
        } else {
            return .unknown
        }
    }
}

final class GithubClient: APIClient {
    private let concreteClient = GithubAPI()
    
    func repositories(by page: Int, completion: @escaping (Result<[Repository], APIError>) -> ()) {
        concreteClient.repositories(by: page).addObserver(owner: self) { [weak self] resource, event in
            self?.process(resource, event: event, with: completion)
        }.loadIfNeeded()
    }
    
    func pullRequests(by owner: String, onRepo repo: String, completion: @escaping (Result<[PullRequest], APIError>) -> ()) {
        concreteClient.pullRequests(by: owner, onRepo: repo).addObserver(owner: self) { [weak self] resource, event in
            self?.process(resource, event: event, with: completion)
        }.loadIfNeeded()
    }
    
    private func process<M>(_ resource: Siesta.Resource, event: Siesta.ResourceEvent, with completion: @escaping (Result<M, APIError>) -> ()) {
        switch event {
            case .error:
                completion(.error(resource.error))
            case .newData(_), .notModified:
                if let results: M = resource.typedContent() {
                    completion(.success(results))
                } else {
                    completion(.error(.adapter))
                }
            default: break
        }
    }
}
