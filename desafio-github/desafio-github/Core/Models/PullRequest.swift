//
//  PullRequest.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation

struct PullRequest {
    
    let id: String
    let title: String
    let body: String?
    let htmlUrl: String?
    let user: User?
}

extension PullRequest: Equatable { }

func ==(lhs: PullRequest, rhs: PullRequest) -> Bool {
    return lhs.id == rhs.id
        && lhs.title == rhs.title
        && lhs.body ==  rhs.body
        && lhs.htmlUrl == rhs.htmlUrl
        && lhs.user == rhs.user
}
