//
//  Repository.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation

struct Repository {
    
    let id: String
    let name: String
    let fullName: String
    let description: String
    let stars: Int
    let forks: Int
    let owner: User?
}

extension Repository: Equatable { }

func ==(lhs: Repository, rhs: Repository) -> Bool {
    return lhs.id == rhs.id
        && lhs.name == rhs.name
        && lhs.fullName == rhs.fullName
        && lhs.description == rhs.description
        && lhs.stars == rhs.stars
        && lhs.forks == rhs.forks
        && lhs.owner == rhs.owner
}
