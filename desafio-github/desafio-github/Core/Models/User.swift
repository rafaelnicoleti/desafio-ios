//
//  User.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation


import Foundation

struct User {
    let id: String
    let login: String
    let email: String?
    let name: String?
    let avatar: String
    let type: String?
}

extension User: Equatable { }

func ==(lhs: User, rhs: User) -> Bool {
    return lhs.id == rhs.id
        && lhs.login == rhs.login
        && lhs.email == rhs.email
        && lhs.name == rhs.name
        && lhs.avatar == rhs.avatar
        && lhs.type == rhs.type
}
