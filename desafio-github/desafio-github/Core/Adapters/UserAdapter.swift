//
//  UserAdapter.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import SwiftyJSON

private extension User {
    struct Keys {
        static let id = "id"
        static let login = "login"
        static let email = "email"
        static let name = "name"
        static let avatar = "avatar_url"
        static let type = "type"
    }
}

final class UserAdapter: Adapter<JSON, User> {
    
    override func adapt() -> Result<User, AdapterError> {
        let id = input[User.Keys.id].stringValue
        let login = input[User.Keys.login].stringValue
        let avatar = input[User.Keys.avatar].stringValue
        
        guard !id.isEmpty && !login.isEmpty && !avatar.isEmpty else {
            return .error(.missingRequiredFields)
        }
        
        let user = User(
            id: id,
            login: login,
            email: input[User.Keys.email].string,
            name: input[User.Keys.name].string,
            avatar: avatar,
            type: input[User.Keys.type].string
        )
        
        return .success(user)
    }
}
