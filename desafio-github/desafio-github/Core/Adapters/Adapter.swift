//
//  Adapter.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation

enum AdapterError: Error {
    case notImplemented
    case missingRequiredFields
}

class Adapter<I, O> {
    
    var input: I
    
    required init(input: I) {
        self.input = input
    }
    
    func adapt() -> Result<O, AdapterError> {
        return Result.error(.notImplemented)
    }
}
