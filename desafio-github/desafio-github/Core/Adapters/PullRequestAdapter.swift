//
//  PullRequestAdapter.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import SwiftyJSON

private extension PullRequest {
    
    struct Keys {
        static let id = "id"
        static let title = "title"
        static let body = "body"
        static let user = "user"
        static let htmlurl = "html_url"
    }
}

final class PullRequestAdapter: Adapter<JSON, PullRequest> {
    
    override func adapt() -> Result<PullRequest, AdapterError> {
        let id = input[PullRequest.Keys.id].stringValue
        let title = input[PullRequest.Keys.title].stringValue
        
        guard !id.isEmpty && !title.isEmpty else {
            return .error(.missingRequiredFields)
        }
        
        let userResult = UserAdapter(input: input[PullRequest.Keys.user]).adapt()
        
        var user: User
        
        switch userResult {
        case .error(let error):
            return .error(error)
        case .success(let userPR):
            user = userPR
        }
        
        let pullRequest = PullRequest(
            id: id,
            title: title,
            body: input[PullRequest.Keys.body].string,
            htmlUrl : input[PullRequest.Keys.htmlurl].string,
            user: user
        )
        
        return .success(pullRequest)
    }
}

final class PullRequestsAdapter: Adapter<JSON, [PullRequest]> {
    
    override func adapt() -> Result<[PullRequest], AdapterError> {
        
        guard let prsArray = input.array else {
            return .error(.missingRequiredFields)
        }
        
        let prs = prsArray.flatMap { prsJSON -> PullRequest? in
            let result = PullRequestAdapter(input: prsJSON).adapt()
            
            switch result {
                case .error(_): return nil
                case .success(let pr): return pr
            }
        }
        
        return .success(prs)
    }
}
