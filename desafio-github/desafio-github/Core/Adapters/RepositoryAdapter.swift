//
//  RepositoryAdapter.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import SwiftyJSON

private extension Repository {
    
    struct Keys {
        static let items = "items"
        static let id = "id"
        static let name = "name"
        static let fullName = "full_name"
        static let description = "description"
        static let stars = "stargazers_count"
        static let forks = "forks"
        static let owner = "owner"
    }
}

final class RepositoryAdapter: Adapter<JSON, Repository> {
    
    override func adapt() -> Result<Repository, AdapterError> {
        let id = input[Repository.Keys.id].stringValue
        let name = input[Repository.Keys.name].stringValue
        let fullName = input[Repository.Keys.fullName].stringValue
        let description = input[Repository.Keys.description].stringValue
        
        guard !id.isEmpty && !name.isEmpty && !fullName.isEmpty && !description.isEmpty else {
            return .error(.missingRequiredFields)
        }
        
        let ownerResult = UserAdapter(input: input[Repository.Keys.owner]).adapt()
        
        var owner: User
        
        switch ownerResult {
        case .error(let error):
            return .error(error)
        case .success(let user):
            owner = user
        }
        
        let repo = Repository(
            id: id,
            name: name,
            fullName: fullName,
            description: description,
            stars: input[Repository.Keys.stars].intValue,
            forks: input[Repository.Keys.forks].intValue,
            owner: owner
        )
        
        return .success(repo)
    }
    
}

final class RepositoriesAdapter: Adapter<JSON, [Repository]> {
    
    override func adapt() -> Result<[Repository], AdapterError> {
        
        guard let reposArray = input[Repository.Keys.items].array else {
            return .error(.missingRequiredFields)
        }
        
        let repos = reposArray.flatMap { repoJSON -> Repository? in
            let result = RepositoryAdapter(input: repoJSON).adapt()
            
            switch result {
                case .error(_): return nil
                case .success(let repo): return repo
            }
        }
        
        return .success(repos)
    }
    
}
