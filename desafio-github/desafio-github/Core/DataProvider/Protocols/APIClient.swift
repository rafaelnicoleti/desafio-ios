//
//  APIClient.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation

enum APIError: Error {
    case unknown
    case http(Error)
    case adapter
}

protocol APIClient {
    func repositories(by page: Int, completion: @escaping (Result<[Repository], APIError>) -> ())
    
    func pullRequests(by owner: String, onRepo repo: String, completion: @escaping (Result<[PullRequest], APIError>) -> ())
}
