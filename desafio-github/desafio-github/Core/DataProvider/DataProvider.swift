//
//  DataProvider.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import RxSwift

public class DataProvider {
    private let client: APIClient
    
    init(client: APIClient) {
        self.client = client
    }
    
    var error = Variable<Error?>(nil)
    
    func repositories(by page: Int) -> Observable<[Repository]> {
        return Observable<[Repository]>.create { o in
            self.client.repositories(by: page) { [weak self] result in
                switch result {
                    case .success(let repos):
                        o.onNext(repos)
                        o.onCompleted()
                    case .error(let error):
                        self?.error.value = error
                    }
            }
            
            return Disposables.create()
        }
    }
    
    func pullRequests(by owner: String, onRepo repo: String) -> Observable<[PullRequest]> {
        return Observable<[PullRequest]>.create { o in
            self.client.pullRequests(by: owner, onRepo: repo) { [weak self] result in
                switch result {
                case .success(let prs):
                    o.onNext(prs)
                    o.onCompleted()
                case .error(let error):
                    self?.error.value = error
                }
            }
            
            return Disposables.create()
        }
    }
}

