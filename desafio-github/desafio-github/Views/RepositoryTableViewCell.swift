//
//  RepositoryTableViewCell.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import CoreGraphics
import UIKit
import Siesta

public class RepositoryTableViewCell : UITableViewCell {
    
    @IBOutlet weak var viewAvatar: UIView!
    @IBOutlet weak var lblNameRepository: UILabel!
    @IBOutlet weak var lblDescriptionRepository: UILabel!
    @IBOutlet weak var lblNameOwner: UILabel!
    @IBOutlet weak var lblLastNameOwner: UILabel!
    @IBOutlet weak var lblNumberForks: UILabel!
    @IBOutlet weak var lblNumberStars: UILabel!
    
    var viewModel: RepositoryViewModel? = nil {
        didSet {
            guard oldValue?.repository != viewModel?.repository else { return }
            
            updateFieldsUI()
        }
    }
    
    private lazy var avatarView: RemoteImageView = {
        let v = RemoteImageView(frame: CGRect(x: 0, y: 0, width: self.viewAvatar.frame.width, height: self.viewAvatar.frame.height))
        
        v.contentMode = .scaleToFill
        
        return v
    }()
    
    private func updateFieldsUI() {
        viewAvatar.addSubview(avatarView)
        avatarView.imageURL = viewModel?.repository.owner?.avatar
        
        lblNameOwner.text = viewModel?.repository.owner?.name
            ?? viewModel?.repository.owner?.login
        lblLastNameOwner.text = viewModel?.repository.owner?.email
            ?? viewModel?.repository.owner?.type
        
        lblNameRepository.text = viewModel?.repository.name
        lblDescriptionRepository.text = viewModel?.repository.description
        
        lblNumberForks.text = viewModel?.repository.forks != nil
            ? "\(viewModel?.repository.forks ?? 0)" : ""
        lblNumberStars.text = viewModel?.repository.forks != nil
            ? "\(viewModel?.repository.stars ?? 0)" : ""
    }
}
