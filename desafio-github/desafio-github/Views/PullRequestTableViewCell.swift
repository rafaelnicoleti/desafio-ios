//
//  PullRequestTableViewCell.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import UIKit
import Siesta

public class PullRequestTableViewCell : UITableViewCell {
    
    @IBOutlet weak var lblTitlePullRequest: UILabel!
    @IBOutlet weak var lblBodyPullRequest: UILabel!
    @IBOutlet weak var lblNameOwner: UILabel!
    @IBOutlet weak var lblTypeOwner: UILabel!
    @IBOutlet weak var veiwAvatarOwner: UIView!
    
    var viewModel: PullRequestViewModel? = nil {
        didSet {
            guard oldValue?.pullRequest != viewModel?.pullRequest else { return }
            
            updateFieldsUI()
        }
    }
    
    private lazy var avatarView: RemoteImageView = {
        let v = RemoteImageView(frame: CGRect(x: 0, y: 0, width: self.veiwAvatarOwner.frame.width, height: self.veiwAvatarOwner.frame.height))
        
        v.contentMode = .scaleToFill
        
        return v
    }()
    
    private func updateFieldsUI() {
        veiwAvatarOwner.addSubview(avatarView)
        avatarView.imageURL = viewModel?.pullRequest.user?.avatar
        
        lblTitlePullRequest.text = viewModel?.pullRequest.title
        lblBodyPullRequest.text = viewModel?.pullRequest.body
        
        lblNameOwner.text = viewModel?.pullRequest.user?.login ?? ""
        lblTypeOwner.text = viewModel?.pullRequest.user?.type ?? ""
    }
}
