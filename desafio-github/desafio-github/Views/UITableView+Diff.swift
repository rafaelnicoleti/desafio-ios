//
//  UITableView+Diff.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import UIKit
import IGListKit

extension UITableView {
    
    func reload(oldData: [ListDiffable], newData: [ListDiffable]) {
        let diff = ListDiffPaths(fromSection: 0, toSection: 0, oldArray: oldData, newArray: newData, option: .equality)
        
        beginUpdates()
        insertRows(at: diff.inserts, with: .top)
        deleteRows(at: diff.deletes, with: .bottom)
        reloadRows(at: diff.updates, with: .none)
        endUpdates()
    }
}
