//
//  PullRequestsViewController.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

public class PullRequestsViewController : UITableViewController {
   
    var owner: String = ""
    var repo: String = ""
    
    private struct Constants {
        static let cellIdentifier = "pullrequestCell"
        static let segueWebView = "segueWebView"
        static let namePullRequestTableViewCell = "PullRequestTableViewCell"
    }
    @IBOutlet var tablePullRequests: UITableView!
    
    private lazy var client: GithubClient = GithubClient()
    
    private var urlPullRequest: String = ""
    
    public lazy var provider: DataProvider = {
        return DataProvider(client: self.client)
    }()
    
    private var pullRequestViewModels: [PullRequestViewModel] = [] {
        didSet {
            tablePullRequests.reload(oldData: oldValue, newData: pullRequestViewModels)
        }
    }
    
    private let disposeBag = DisposeBag()
    
    override public func viewDidLoad() {
        tablePullRequests.register(UINib(nibName: Constants.namePullRequestTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
        
        configureObservables()
    }
    
    func configureObservables() {
        self.provider.pullRequests(by: self.owner, onRepo: self.repo).observeOn(MainScheduler.instance).subscribe { event in
            switch event {
                case .next(let prs):
                    if !prs.isEmpty {
                        self.pullRequestViewModels = prs.map(PullRequestViewModel.init)
                    }
                default: break
            }
        }.disposed(by: self.disposeBag)
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pullRequestViewModels.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier, for: indexPath) as! PullRequestTableViewCell
        
        cell.viewModel = pullRequestViewModels[indexPath.row]
        
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        urlPullRequest = pullRequestViewModels[indexPath.row].pullRequest.htmlUrl!
        
        self.performSegue(withIdentifier: Constants.segueWebView, sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let webViewViewController = segue.destination as! WebViewPRViewController
        
        webViewViewController.urlPullRequest = urlPullRequest
    }
}
