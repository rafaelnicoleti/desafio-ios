//
//  RepositoryViewController.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import IGListKit

public class RepositoryViewController : UITableViewController {
    private struct Constants {
        static let cellIdentifier = "repositoryCell"
        static let seguePullRequest = "seguePullRequest"
        static let nameRepositoryTableViewCell = "RepositoryTableViewCell"
    }
    
    @IBOutlet var tableRepository: UITableView!
    
    private lazy var client: GithubClient = GithubClient()
    private var nextPage: Int = 1
    private var ownerSelected: String = ""
    private var repoSelected: String = ""
    
    public lazy var provider: DataProvider = {
        return DataProvider(client: self.client)
    }()
    
    private var repositoryViewModels: [RepositoryViewModel] = [] {
        didSet {
            tableRepository.reload(oldData: oldValue, newData: repositoryViewModels)
        }
    }
    
    private let disposeBag = DisposeBag()
    
    override public func viewDidLoad() {
        tableRepository.register(UINib(nibName: Constants.nameRepositoryTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.cellIdentifier)
        
        configureObservables()
    }
    
    func configureObservables() {
        self.provider.repositories(by: self.nextPage).observeOn(MainScheduler.instance).subscribe { event in
            switch event {
                case .next(let repositories):
                    if !repositories.isEmpty {
                        self.repositoryViewModels = repositories.map(RepositoryViewModel.init)
                    }
                default: break
            }
        }.disposed(by: self.disposeBag)
        
        let loadNextPage = tableView.rx.didScroll.debounce(0.3, scheduler: MainScheduler.instance)
            .flatMap { () -> Observable<[Repository]> in
                self.nextPage += 1
                return self.provider.repositories(by: self.nextPage)
        }
        
        loadNextPage.observeOn(MainScheduler.instance).subscribe { event in
            switch event {
            case .next(let repositories):
                if !repositories.isEmpty {
                    let result = repositories.map(RepositoryViewModel.init)
                    self.repositoryViewModels.append(contentsOf: result)
                }
            default: break
            }
        }.disposed(by: self.disposeBag)
    }
    
    override public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return repositoryViewModels.count
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellIdentifier, for: indexPath) as! RepositoryTableViewCell
        
        cell.viewModel = repositoryViewModels[indexPath.row]
        
        return cell
    }
    
    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        ownerSelected = repositoryViewModels[indexPath.row].repository.owner?.login ?? ""
        repoSelected =  repositoryViewModels[indexPath.row].repository.name
        
        self.performSegue(withIdentifier: Constants.seguePullRequest, sender: self)
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let pullRequestsViewController = segue.destination as! PullRequestsViewController

        pullRequestsViewController.owner = ownerSelected
        pullRequestsViewController.repo = repoSelected
    }
}

extension UIScrollView {
    func  isNearBottomEdge(edgeOffset: CGFloat = 20.0) -> Bool {
        return self.contentOffset.y + self.frame.size.height + edgeOffset > self.contentSize.height
    }
}
