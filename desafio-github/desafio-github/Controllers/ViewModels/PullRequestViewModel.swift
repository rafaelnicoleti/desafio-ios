//
//  PullRequestViewModel.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import IGListKit

final class PullRequestViewModel: NSObject, ListDiffable {
    
    let pullRequest: PullRequest
    
    init(pullRequest: PullRequest) {
        self.pullRequest = pullRequest
        
        super.init()
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return pullRequest.id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let other = object as? PullRequestViewModel else { return false }
        
        return self.pullRequest == other.pullRequest
    }
}
