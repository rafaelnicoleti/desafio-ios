//
//  RepositoryViewModel.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 18/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import IGListKit

final class RepositoryViewModel: NSObject, ListDiffable {
    
    let repository: Repository
    
    init(repository: Repository) {
        self.repository = repository
        
        super.init()
    }
    
    func diffIdentifier() -> NSObjectProtocol {
        return repository.id as NSObjectProtocol
    }
    
    func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
        guard let other = object as? RepositoryViewModel else { return false }
        
        return self.repository == other.repository
    }
}
