//
//  WebViewPRViewController.swift
//  desafio-github
//
//  Created by Rafael Nicoleti on 20/01/18.
//  Copyright © 2018 Rafael Nicoleti. All rights reserved.
//

import Foundation
import UIKit
import WebKit

public class WebViewPRViewController : UIViewController {
    var urlPullRequest: String = ""
    
    @IBOutlet var webView: WKWebView!
    
    override public func viewDidLoad() {
        let myURL = URL(string: urlPullRequest)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
    }
}
